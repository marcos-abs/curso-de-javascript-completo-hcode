/*
 * File: index.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Tuesday, 28 April 2020 17:37:15
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Tuesday, 28 April 2020 17:37:15
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

window.app = new DropBoxController();