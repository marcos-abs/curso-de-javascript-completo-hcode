/*
 * File: index.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Thursday, 30 April 2020 13:03:34
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Monday, 04 May 2020 21:52:00
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

var express = require('express');
var router = express.Router();
var formidable = require('formidable');
var fs = require('fs'); // File System

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/file', (req, res) => {
  let path = './' + req.query.path;
  if (fs.existsSync(path)) {
    fs.readFile(path, (err, data) => {
      if (err) {
        console.log('err: ', err);
        res.status(400).json({
          error: err
        });
      } else {
        res.status(200).end(data);
      }
    });
  } else {
    res.status(404).json({
      error: 'File not found.'
    });
  }
});

router.delete('/file', (req, res) => {
  let form = new formidable.IncomingForm({
    uploadDir: './upload',
    keepExtensions: true
  });
  form.parse(req, (err, fields, files) => {
    let path = "./" + fields.path;
    console.log('path: ', path);
    if(fs.existsSync(path)) {
      fs.unlink(path, err => { // comando de deleção com o FS.
        if(err) {
          res.status(400).json({ // retorno no formato REST q ñ conseguiu deletar.
            err
          });
        } else { // caso contrário, mostra os arquivos que restaram.
          res.json({
            fields
          });
        }
      });
    } else {
      res.status(404).json({
        error: 'File not found.'
      });
    }
  });
});

router.post('/upload', (req, res) => {
  let form = new formidable.IncomingForm({
    uploadDir: './upload',
    keepExtensions: true
  });
  form.parse(req, (err, fields, files) => {
    res.json({
      files
    });
  });
});

module.exports = router;


