/*
 * File: Utils.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Thursday, 16th April 2020 5:17:00 pm
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Thursday, 16-04-2020 17:20:30
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */
class Utils {
    static dateFormat(date) {
        return date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear()+' '+date.getHours()+':'+date.getMinutes();
    }
}