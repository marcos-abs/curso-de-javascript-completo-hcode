/**
 * File: DocumentPreviewController.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Wednesday, 15 July 2020 23:07:35
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 21 October 2020 14:26:03
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2020 All rights reserved, Marcant Tecnologia
 * -----
 */

// *** IMPORTANTE: *** essa library PDFJS não funciona com o live-server do vscode.
const pdfjsLib = require('pdfjs-dist'); // fonte: https://github.com/mozilla/pdf.js
const path = require('path');

pdfjsLib.GlobalWorkerOptions.workerSrc = path.resolve(__dirname, '../../dist/pdf.worker.bundle.js');

export class DocumentPreviewController {
    constructor(file) {
        this._file = file;
    }

    getPreviewData() {
        return new Promise((s, f) => {
            let reader = new FileReader();
            switch (this._file.type) {
                case 'image/png':
                case 'image/jpeg':
                case 'image/jpg':
                case 'image/gif':
                    reader.onload = e => {
                        s({
                            src: reader.result,
                            info: this._file.name
                        });
                    };
                    reader.onerror = e => {
                        f(e);
                    }
                    reader.readAsDataURL(this._file);
                break;

                case 'application/pdf':
                    reader.onload = e => {
                        pdfjsLib.getDocument(new Uint8Array(reader.result)).then(pdf => {
                            pdf.getPage(1).then(page => {
                                let viewport = page.getViewport(1);

                                let canvas = document.createElement('canvas');
                                let canvasContext = canvas.getContext('2d');

                                canvas.width = viewport.width;
                                canvas.height = viewport.height;

                                page.render({
                                    canvasContext,
                                    viewport
                                }).then(() => {
                                    let _s = (pdf.numPages > 1) ? 's':'';
                                    s({
                                        src: canvas.toDataURL('image/png'),
                                        info: `${pdf.numPages} página${_s}`
                                    });
                                }).catch(err => {
                                    f(err);
                                });
                            }).catch(err => {
                                f(err);
                            });
                        }).catch(err => {
                            f(err);
                        });
                    };
                    reader.readAsArrayBuffer(this._file);
                break;
                default:
                    f();
            }
        });
    }
}