/*
 * File: MicrophoneController.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Wednesday, 03 June 2020 14:57:07
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Wednesday, 03 June 2020 15:00:10
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

import { ClassEvent } from "../util/ClassEvent";

export class MicrophoneController extends ClassEvent {
    constructor() {
        super(); // chama o construtor da classe "pai" (extends - herança).

        this._mimeType = 'audio/webm';

        this._available = false; // flag da concessão da permissão de gravação de audio pelo navegador.
        navigator.mediaDevices.getUserMedia({
            audio: true
        }).then(stream => {
            this._available = true;
            this._stream = stream; // alimentando o escopo da variável para acessá-la de fora do escopo atual.
            // let audio = new Audio();
            // //audio.src = URL.createObjectURL(stream);
            // audio.srcObject = stream;
            // audio.play();
            // this.trigger('ready', audio);
            this.trigger('ready', this._stream);

        }).catch(err => {
            console.error(err);
        });

    }

    isAvailable() {
        return this._available;
    }

    stop () {
        this._stream.getTracks().forEach(track => {
            track.stop();
        });
    }

    startRecorder() {
        if(this.isAvailable()) {
            this._mediaRecorder = new MediaRecorder(this._stream, {
                _mimeType: this._mimeType
            });

            this._recordedChunks = [];

            this._mediaRecorder.addEventListener('dataavailable', e => {
                if(e.data.size > 0) this._recordedChunks.push(e.data);

            });

            this._mediaRecorder.addEventListener('stop', e => {
                let blob = new Blob(this._recordedChunks, {
                    type: this._mimeType
                });
                let filename = `rec${Date.now()}.webm`;
                let file = new File([blob], filename, {
                    type: this._mimeType,
                    lastModified: Date.now()
                });
                console.log('file: ', file);

                /**-------------------------------------------------*
                 *  Somente para validar se o audio está gravando.  |
                 *--------------------------------------------------*
                 * let reader = new FileReader();
                 * reader.onload = e => {
                 *      console.log('reader file: ', file);
                 *      let audio = new Audio(reader.result);
                 *      audio.play();
                 * }
                 * reader.readAsDataURL(file);
                 */
            });
            this._mediaRecorder.start();
            this.startTimer();
        }
    }

    stopRecorder() {
        if(this.isAvailable()) {
            this._mediaRecorder.stop();
            this.stop();
            this.stopTimer();
        }
    }

    startTimer() {
        let start = Date.now();
        this._recordMicrophoneInterval = setInterval(() => {
            this.trigger('recordtimer',(Date.now() - start));
        }, 100);
    }

    stopTimer() {
        clearInterval(this._recordMicrophoneInterval);
    }
}