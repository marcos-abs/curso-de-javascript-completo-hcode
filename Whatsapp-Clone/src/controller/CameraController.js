/**
 * File: CameraController.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Thursday, 07 May 2020 18:46:18
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Wednesday, 30 September 2020 17:29:24
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

 export class CameraController {
    constructor(videoEl) {

        this._videoEl = videoEl;

        navigator.mediaDevices.getUserMedia({
            video: true
        }).then(stream => {
            this._stream = stream; // alimentando o escopo da variável para acessá-la de fora do escopo atual.
            this._videoEl.srcObject = new MediaStream(stream); // Valew @CésarAugustoVazSampaioFilho
            this._videoEl.play();
        }).catch(err => {
            console.error(err);
        });
    }

    stop () {
        this._stream.getTracks().forEach(track => {
            track.stop();
        });
    }

    takePicture(mimeType = 'image/png') {
        let canvas = document.createElement('canvas');

        canvas.setAttribute('height', this._videoEl.videoHeight);
        canvas.setAttribute('width', this._videoEl.videoWidth);

        let context = canvas.getContext('2d');

        context.drawImage(this._videoEl, 0, 0, canvas.width, canvas.height);

        return canvas.toDataURL(mimeType);
    }
}