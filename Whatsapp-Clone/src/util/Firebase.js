/**
 * File: Firebase.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Wednesday, 15 July 2020 23:07:35
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Thursday, 29 October 2020 12:19:53
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2020 All rights reserved, Marcant Tecnologia
 * -----
 */

import { ResolvePlugin } from 'webpack';

const firebase = require('firebase/app');
require('firebase/auth');
require('firebase/firestore');

export class Firebase {
    constructor() {
        this._config = {
            apiKey: "AIzaSyDULwsKbQcxLlfKWQTZZ8v54F9lcjrnBlU",
            authDomain: "whatsapp-clone-65718.firebaseapp.com",
            databaseURL: "https://whatsapp-clone-65718.firebaseio.com",
            projectId: "whatsapp-clone-65718",
            storageBucket: "gs://whatsapp-clone-65718.appspot.com",
            messagingSenderId: "447036720580",
            appId: "1:447036720580:web:509c888f84ef560d63d091"
        };
        this.init();
    }

    init() {
        // Initialize Firebase
        if(!window._initializedFirebase) {
            firebase.initializeApp(this._config);
            /**
             * Erro: The setting 'timestampsInSnapshots: true' is no longer required and should be removed
             *
             * firebase.firestore().settings({
             *     timestampsInSnapshots: true // para uso intensivo (constantemente).
             * });
             */
            firebase.firestore().settings({}); // fonte: https://www.udemy.com/course/javascript-curso-completo/learn/lecture/10507766#questions/10265276
            window._initializedFirebase = true;
        }
    }

    static db() {
        return firebase.firestore();
    }

    static hd() {
        return firebase.storage();
    }

   initAuth() {
        return new Promise((resolve, reject) => {
            let provider = new firebase.auth.GoogleAuthProvider();
            firebase.auth()
                .signInWithPopup(provider)
                .then(function (result) {
                    let token = result.credential.accessToken;
                    let user = result.user;
                    resolve(user, token);
                }).catch(error => {
                    reject(error);
                });
        });
    }
}