/*
 * File: ClassEvent.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Wednesday, 03 June 2020 15:10:44
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Wednesday, 03 June 2020 15:10:45
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

export class ClassEvent {
    constructor() {
        this._events = {};
    }

    on(eventName, fn) {
        if(!this._events[eventName]) this._events[eventName] = new Array();

        this._events[eventName].push(fn);
    }

    trigger() {
        let args = [...arguments];
        let eventName = args.shift();

        args.push(new Event(eventName));

        if(this._events[eventName] instanceof Array) {
            this._events[eventName].forEach(fn => {
                fn.apply(null, args); // executa o conteúdo da variável.
            });
        }
    }
}