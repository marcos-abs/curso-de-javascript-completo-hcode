/**
 * File: app.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Wednesday, 15 July 2020 23:07:35
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 23 October 2020 11:25:43
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2020 All rights reserved, Marcant Tecnologia
 * -----
 */

import {WhatsAppController} from './controller/WhatsAppController';

if (process.env.NODE_ENV === 'production') {
  app.set('trust proxy', 1); // trust first proxy
  sessionConfig.cookie.secure = true; // serve secure cookies
}

window.app = new WhatsAppController();
