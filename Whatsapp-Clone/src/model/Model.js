/**
 * File: Model.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Wednesday, 15 July 2020 23:07:35
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 23 October 2020 11:18:52
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2020 All rights reserved, Marcant Tecnologia
 * -----
 */
/*
 * File: Model.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Thursday, 11 June 2020 12:33:50
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Thursday, 11 June 2020 12:41:14
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

import {ClassEvent} from "../util/ClassEvent";

export class Model extends ClassEvent {
    constructor() {
        super();
        this._data = {};
    }

    fromJSON(json) {
        this._data = Object.assign(this._data, json); // concatenar objetos.
        this.trigger('datachange', this.toJSON()); // avisar eventos 'trigger' que houve alteração no conteúdo da variável.
    }

    toJSON() {
        return this._data;
    }
}