/*
 * File: Chat.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Friday, 19 June 2020 14:06:08
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Sunday, 26 July 2020 16:03:32
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */
import { Model } from './Model';
import { Firebase } from '../util/Firebase';

export class Chat extends Model {
    constructor() {
        super();
    }

    get users() { return this._data.users; }
    set users(value) { this._data.users = value; }

    get timeStamp() { return this._data.timeStamp; }
    set timeStamp(value) { this._data.timeStamp = value; }

    static getRef() {
        return Firebase.db().collection('/chats');
    }

    static create(meEmail, contactEmail) {
        return new Promise((s, f) => {

            let users = {};

            users[btoa(meEmail)] = true;
            users[btoa(contactEmail)] = true;

            Chat.getRef().add({
                users,
                timeStamp: new Date()
            }).then(doc => {
                Chat.getRef().doc(doc.id).get().then(chat => {
                    s(chat);
                }).catch(err => { f(err); });
            }).catch(err => { f(err); });
        });
    }

    static find(meEmail, contactEmail) {
        return Chat.getRef()
            .where(btoa(meEmail), '==', true)
            .where(btoa(contactEmail), '==', true)
            .get();
    }

    static createIfNotExist(meEmail, contactEmail) {
        return new Promise((s, f) => {
            Chat.find(meEmail, contactEmail).then(chats => {
                if (chats.empty) {
                    Chat.create(meEmail, contactEmail).then(chat => {
                        s(chat);
                    });
                } else {
                    chats.forEach(chat => {
                        s(chat);
                    });
                }
            }).catch(err => { f(err); });
        });
    }
}