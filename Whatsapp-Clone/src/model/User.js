/**
 * File: User.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Wednesday, 15 July 2020 23:07:35
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Friday, 23 October 2020 12:22:32
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2020 All rights reserved, Marcant Tecnologia
 * -----
 */

import { Firebase } from './../util/Firebase';
import { Model } from './Model';

export class User extends Model {

    constructor(id) {
        super(); // constructor do "ClassEvent".
        this._data = {}; // desnecessário.
        if(id) this.getById(id);
    }

    get name() { return this._data.name; }
    set name(value) { this._data.name = value; }

    get email() { return this._data.email; }
    set email(value) { this._data.email = value; }

    get photo() { return this._data.photo; }
    set photo(value) { this._data.photo = value; }

    get chatId() { return this._data.chatId; }
    set chatId(value) { this._data.chatId = value; }

    getById(id) {
        return new Promise((s, f) => {
            User.findByEmail(id).onSnapshot(doc => {
                this.fromJSON(doc.data());
                s(doc);
            });
        });
    }

    save() {
        return User.findByEmail(this.email).set(this.toJSON());
    }

    static getRef() {
        return Firebase.db().collection('/users');
    }

    static getContactsRef(id) {
        return User.getRef()
            .doc(id)
            .collection('contacts');

    }

    static findByEmail(email) {
        return User.getRef().doc(email);
    }

    addContact(contact) {
        return User.getContactsRef(this.email)
            .doc(btoa(contact.email))  // btoa() ==>> função de conversão de texto para base64 (!!) o contrário é atob() de base64 para texto (!!).
            .set(contact.toJSON());
    }

    getContacts(filter = '') {
        return new Promise((s, f) => {
            User.getContactsRef(this.email).where('name', '>=', filter).onSnapshot(docs => {
                let contacts = [];
                docs.forEach(doc => {
                    let data = doc.data();
                    data.id = doc.id;
                    contacts.push(data);
                });
                this.trigger('contactschange', docs);
                s(contacts);
            });
        });
    }
}