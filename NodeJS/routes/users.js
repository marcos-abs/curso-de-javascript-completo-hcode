/*
 * File: users.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Tuesday, 21 April 2020 11:57:32
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Wednesday, 22 April 2020 12:43:47
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

let NeDB = require('nedb');
let db = new NeDB({
    filename:'users.db',
    autoload:true
});

module.exports = (app) => {
    let route = app.route('/users');
    route.get((req, res) => {

        db.find({}).sort({name:1}).exec((err, users) => {  // sort: 1 = ascendent, -1 = descendent
            if (err) {
                app.utils.error.send(err, req, res); // app.utils.error.send(err, req, res, 400); // 400 é padrão, então pode ser removido dos argumentos.
            } else {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json'); // Header do HTML dizendo que tipo de informação será passada pelo o servidor.
                res.json({
                    users // users: users // ECMA6: nome do banco de dados = nome da instancia
                });
            }
        });
    });
    
    route.post((req, res) => {
        if (!app.utils.validator.user(app, req, res)) return false; //validações de campos com 'express-validator'.
        db.insert(req.body, (err, user) => {
            if(err) {
                app.utils.error.send(err, req, res);
            } else {
                res.status(200).json(user);
            }
        });
    });

    let routeId = app.route('/users/:id');

    routeId.get((req, res) => {
        db.findOne({_id:req.params.id}).exec((err, user) => {
            if(err) {
                app.utils.error.send(err, req, res);
            } else {
                res.status(200).json(user);
            }
        });
    });

    routeId.put((req, res) => {
        if (!app.utils.validator.user(app, req, res)) return false; //validações de campos com 'express-validator'.
        db.update({_id:req.params.id}, req.body, err => {
            if(err) {
                app.utils.error.send(err, req, res);
            } else {
                res.status(200).json(Object.assign(req.params, req.body));
            }
        });
    });

    routeId.delete((req, res) => {
        db.remove({_id:req.params.id}, {}, err => {
            if(err) {
                app.utils.error.send(err, req, res);
            } else {
                res.status(200).json(req.params);
            }
        });
    });
}
