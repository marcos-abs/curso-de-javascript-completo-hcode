/*
 * File: error.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Wednesday, 22 April 2020 12:59:51
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Wednesday, 22 April 2020 12:59:52
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

 module.exports = {
    send: (err, req, res, code = 400) => {
        console.log(`error: ${err}`);
        res.status(code).json({
            error:err
        });
    }
 };