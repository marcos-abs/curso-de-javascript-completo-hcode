/*
 * File: calcController.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Saturday, 11th April 2020 10:28:00 am
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Saturday, 12-04-2020 15:43:50
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */
class CalcController {

    constructor() {
        this._audio = new Audio('click.mp3');
        this._audioOnOff = false;
        this._lastOperator = '';
        this._lastNumber = '';
        this._operation = [];
        this._locale = 'pt-BR';
        this._displayCalcEl = document.querySelector("#display"); // coleta a informação e armazena na variável
        this._dateEl = document.querySelector("#data"); // coleta a informação e armazena na variável
        this._timeEl = document.querySelector("#hora"); // coleta a informação e armazena na variável
        this._currentDate;

        this.initialize();
        this.initButtonsEvents();
        this.initKeyboard();
    }

    pasteFromClipboard () {
        document.addEventListener('paste', e => {
            let text = e.clipboardData.getData('Text'); // pega qualquer valor letras ou números, ou seja, diferente de imagens (por exemplo).
            this.displayCalc = parseFloat(text);
        });
    }

    copyToClipboard () {
        let input = document.createElement('input'); // criando um campo de input no body HTML
        input.value = this.displayCalc; // copiando os valores do Display da Calculadora para o campo de input - necessário para que funcione o CTRL+C e depois o CTRL+V
        document.body.appendChild(input); // adicionando o input ao Body HTML.
        input.select(); // selecionando o conteúdo do HTML.
        document.execCommand("Copy"); // executando o Copy para a Área de Tranferência do Sistema Operacional.
        input.remove(); // removendo da tela o campo que já não é mais necessário.
    }

    initialize() {
        this.setDisplayDateTime();
        setInterval(() => {
            this.setDisplayDateTime();
        }, 1000);
        this.setLastNumberToDisplay();
        this.pasteFromClipboard();

        document.querySelectorAll('.btn-ac').forEach(btn => {
            btn.addEventListener('dblclick', e => {
                this.toogleAudio();
            });
        });
    }

    toogleAudio() {
        this._audioOnOff = !this._audioOnOff; // Massa! (!!)

    }

    playAudio() {
        if (this._audioOnOff) {
            this._audio.currentTime = 0; // rebobina o audio (rs)
            this._audio.play(); 
        }
    }

    addEventListenerAll(element, events, fn) { // refatoração do addEventListener para que não tenham vários blocos de código repetidos.
        events.split(' ').forEach(event => {
            element.addEventListener(event, fn, false);
        });
    }

    clearAll() {
        this._operation = [];
        this._lastNumber = '';
        this._lastOperator = '';
        this.setLastNumberToDisplay();
    }

    clearEntry() {
        this._operation.pop();
        this.setLastNumberToDisplay();
    }

    setError() {
        this.displayCalc = "Error";
    }

    isOperator(value) {
        return (['+', '-', '*', '%', '/'].indexOf(value) > -1); // Massa! (!!)
    }

    pushOperation(value) {
        this._operation.push(value);
        if(this._operation.length > 3) {
            this.calc();
        }
    }

    getResult () {
        try {
            return eval(this._operation.join(""));
        } catch(e) {
            setTimeout(() => {
                this.setError();
            }, 1);
        }
        
    }

    calc() {
        let last = '';
        this._lastOperator = this.getLastItem();
        if (this._operation.length < 3) {
            let firstItem = this._operation[0];
            this._operation = [firstItem, this._lastOperator, this._lastNumber];
        }
        if (this._operation.length > 3) {
            last = this._operation.pop();
            this._lastNumber = this.getResult();
        } else if (this._operation.length == 3) {
            this._lastNumber = this.getLastItem(false);
        }
        let result = this.getResult();
        if (last == '%') {
            result /= 100; // result = result / 100;
            this._operation = [ result ];
        } else {
            this._operation = [ result ];
            if (last) this._operation.push(last);
        }
        this.setLastNumberToDisplay();
    }

    getLastItem(isOperator = true) {
        let lastItem;
        for (let i = this._operation.length - 1; i >= 0; i--) {
            if (this.isOperator(this._operation[i]) == isOperator) {
                lastItem = this._operation[i];
                break;
            }
        }
        if (!lastItem) {
            lastItem = (isOperator) ? this._lastOperator : this._lastNumber; // Massa ^2 (!!)
        }
        return lastItem;
    }

    setLastNumberToDisplay() {
        let lastNumber = this.getLastItem(false);
        if (!lastNumber) lastNumber = 0;
        this.displayCalc = lastNumber;
    }

    addOperation(value) {
        if(isNaN(this.getLastOperation())) { // último caracter da operação é um número ?
            // última operação é um string ou nula (não é um número)
            if (this.isOperator(value)) { // último caracter digitado é um operador ?
                // trocar o operador
                this.setLastOperation(value); // grava na operação a ultima operação.

            } else {
                // é um número
                //this.setLastOperation(parseInt(value));
                this.pushOperation(value);
                this.setLastNumberToDisplay();
            }
        } else {
            if (this.isOperator(value)) {
                this.pushOperation(value);
            } else {
                // o caracter atual é um numero
                let newValue = this.getLastOperation().toString() + value.toString();
                this.setLastOperation(newValue);

                this.setLastNumberToDisplay();
            }
        }
    }

    getLastOperation() {
        return this._operation[this._operation.length-1];
    }

    setLastOperation(value) {
        this._operation[this._operation.length-1] = value;
    }

    addDot() {
        let lastOperation = this.getLastOperation();
        if (typeof lastOperation === 'string' && lastOperation.split('').indexOf('.') > -1) return;
        if (this.isOperator(lastOperation) || !lastOperation) {
            this.pushOperation('0.');
        } else {
            this.setLastOperation(lastOperation.toString() + '.');
        }
        this.setLastNumberToDisplay();
    }

    execBtn(value) {
        this.playAudio(); // toca o audio da tecla pressionada.
        switch(value) {
            case 'ac':
                this.clearAll();
                break;
            case 'ce':
                this.clearEntry();
                break;
            case 'soma':
                this.addOperation('+');
                break;
            case 'subtracao':
                this.addOperation('-');
                break;
            case 'divisao':
                this.addOperation('/');
                break;
            case 'multiplicacao':
                this.addOperation('*');
                break;
            case 'porcento':
                this.addOperation('%');
                break;
            case 'igual':
                this.calc();
                break;
            case 'ponto':
                this.addDot();
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                this.addOperation(parseInt(value));
                break;
            case 'c':
                if (e.ctrlKey) this.copyToClipboard();
                break;
            default:
                this.setError();
        }
    }

    initKeyboard() {
        document.addEventListener('keyup', e => {
            this.playAudio(); // toca o audio da tecla pressionada.
            switch(e.key) {
                case 'Escape':
                    this.clearAll();
                    break;
                case 'Backspace':
                    this.clearEntry();
                    break;
                case '+':
                case '-':
                case '/':
                case '*':
                case '%':
                    this.addOperation(e.key);
                    break;
                case 'Enter':
                case '=':
                    this.calc();
                    break;
                case '.':
                case ',':
                    this.addDot();
                    break;
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    this.addOperation(parseInt(e.key));
                    break;
                case 'c':
                    if (e.ctrlKey) this.copyToClipboard();
                    break;
            }
        });
    }

    initButtonsEvents() {
        let buttons = document.querySelectorAll("#buttons > g, #parts > g");

        buttons.forEach((btn, index) => {
            this.addEventListenerAll(btn, 'click drag', e => {
                let textBtn = (btn.className.baseVal.replace("btn-", "")); // remover o "btn-" do nome da classe para aparecer somente o valor da tecla (!!).
                this.execBtn(textBtn);

            });
            this.addEventListenerAll(btn, 'mouseover mouseup mousedown', e => {
                btn.style.cursor = "pointer";
            });
        });

    }

    setDisplayDateTime() {
        this.displayDate = this.currentDate.toLocaleDateString(this._locale, {
            day: "2-digit",
            month: "long",
            year: "numeric"
        });
        this.displayTime = this.currentDate.toLocaleTimeString(this._locale);
    }

    get displayTime() {
        return this._timeEl.innerHTML;
    }

    set displayTime(valor) {
        return this._timeEl.innerHTML = valor;
    }

    get displayDate() {
        return this._dateEl.innerHTML;
    }

    set displayDate(valor) {
        if (valor.toString.length > 10) {
            this.setError();
            return false;
        }
        return this._dateEl.innerHTML = valor;
    }

    get displayCalc() {
        return this._displayCalcEl.innerHTML;
    }

    set displayCalc(valor) {
        this._displayCalcEl.innerHTML = valor;
    }

    get currentDate() {
        return new Date();
    }

    set dataAtual(valor) {
        this._currentDate = valor;
    }
}