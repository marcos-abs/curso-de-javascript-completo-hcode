/*
 * File: calculator.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Saturday, 11th April 2020 10:12:37 am
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Saturday, 11-04-2020 10:24:02
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */
window.calculator = new CalcController();
