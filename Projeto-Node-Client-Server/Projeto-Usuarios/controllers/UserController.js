/*
 * File: UserController.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Wednesday, 15 April 2020 15:08:27
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Monday, 20 April 2020 09:39:08
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */
class UserController {
    

    constructor(formIdCreate, formIdUpdate, tableId) {
        this.formCreateE1 = document.getElementById(formIdCreate);
        this.formUpdateE1 = document.getElementById(formIdUpdate);
        this.tableE1 = document.getElementById(tableId);

        this.onSubmit(); // adicionando a função Submit ao botão do formulário.
        this.onEdit(); // adicionando a função Cancel (dentro d edição - update) ao botão do formulário.
        this.selectAll();
    }

    onSubmit() {
        this.formCreateE1.addEventListener("submit", event => { // o ArrowFunction mantém o escopo do this para a classe toda, ao contrário do function que cria um novo escopo.
            event.preventDefault(); // cancela o comportamento padrão, ou seja, neste caso recarrega a página.
            let btn = this.formCreateE1.querySelector("[type=submit]"); // seleciona o botão no HTML para desabilitar e habilitar.
            btn.disabled = true; // desabilita o botão de salvar (para evitar salvar vários registros clicando várias vezes).
            let values = this.getValues(this.formCreateE1);
            if(!values) return false;
            this.getPhoto(this.formCreateE1).then( (content) => {
                // se der certo.
                values.photo = content;
                values.save();
                this.addLine(values); 
                this.formCreateE1.reset(); // apaga os valores do formulário para um novo cadastro.
                btn.disabled = false; // habilita o botão de salvar (condição default para iniciar o cadastro).
                }, (e) => {
                    // se der errado.
                    console.error(e);
            });
        });
    }

    onEdit() {
        document.querySelector("#box-user-update .btn-cancel").addEventListener("click", e => {
            this.showPanelCreate();
        });
        this.formUpdateE1.addEventListener("submit", event =>{
            event.preventDefault(); // cancela o comportamento padrão, ou seja, neste caso recarrega a página.
            let btn = this.formUpdateE1.querySelector("[type=submit]"); // seleciona o botão no HTML para desabilitar e habilitar.
            btn.disabled = true; // desabilita o botão de salvar (para evitar salvar vários registros clicando várias vezes).
            let values = this.getValues(this.formUpdateE1);
            let index = this.formUpdateE1.dataset.trIndex;
            let tr = this.tableE1.rows[index];
            let userOld = JSON.parse(tr.dataset.user);
            let result = Object.assign({}, userOld, values);
            this.showPanelCreate();
            this.getPhoto(this.formUpdateE1).then( (content) => {
                if (!values.photo) {
                    result._photo = userOld._photo;  // se a foto não foi alterada, "não" vamos substituir por nada (sic).
                } else {
                    result._photo = content; // mantem a foto que foi trocada/modificada.
                }
                let user = new User();
                user.loadFromJSON(result);
                user.save();
                this.getTr(user, tr); // código refatorado.
                this.updateCount();
                this.formUpdateE1.reset(); // zerar os valores do formulário de edição.
                btn.disabled = false; // Habilita (novamente) o botão de salvar.
                this.showPanelCreate();
                }, (e) => {
                    // se der errado.
                    console.error(e);
            });
        });
    }

    getPhoto(formE1) {

        return new Promise((resolve, reject)=>{
            let fileReader = new FileReader();
            let elements = [...formE1.elements].filter(item => {
                if (item.name === 'photo') {
                    return item;
                }
            });
    
            let file = elements[0].files[0];
    
            fileReader.onerror = (e) => {
                reject(e);
            }

            fileReader.onload = () => {
                resolve(fileReader.result);
            }

            if(file) {
                fileReader.readAsDataURL(file);
            } else {
                resolve('dist/img/boxed-bg.jpg');
            }
        });
    }
    
    

    getValues(formE1) { // este argumento (formE1) é local, não confundir com o formCreateE1 e o formUpdateE1.
        let user = {};
        let isValid = true; // flag para controle do fluxo do programa (para "escapar" da rotina).

        [...formE1.elements].forEach(function(field, index) { // colchetes são para converter o "object" em array. E o "spread", ou seja as reticencias (...) eliminam a necessidade de "saber" quantos ou quais índices serão convertidos.
            if (['name', 'email', 'password'].indexOf(field.name) > -1 && !field.value) {
                field.parentElement.classList.add('has-error');
                isValid = false;
            }
            if (field.name == "gender") {
                if (field.checked) { //if (field.checked === true) {
                    user[field.name] = field.value;
                }
            } else if (field.name == "admin") {
                user[field.name] = field.checked;
            } else {
                user[field.name] = field.value;
            }
        });
        
        if(!isValid) return false; // termina a execução caso um dos campos requeridos não seja preenchido.

        return  new User(
            user.name, user.gender, user.birth, user.country, 
            user.email, user.password, user.photo, user.admin
        );
    }

    selectAll() {
        let users = User.getUsersStorage();
        users.forEach(dataUser => {
            let user = new User();
            user.loadFromJSON(dataUser);
            this.addLine(user);
        });
    }
    
    addLine(dataUser) {
        let tr = this.getTr(dataUser); // código refatorado.
        this.tableE1.appendChild(tr); 
        this.updateCount();
    }

    getTr(dataUser, tr = null) {
        if (tr === null) tr = document.createElement('tr');
        tr.dataset.user = JSON.stringify(dataUser); // Serialize by JSON.
        tr.innerHTML = `
            <td><img src="${dataUser.photo}" alt="User Image" class="img-circle img-sm"></td>
            <td>${dataUser.name}</td>
            <td>${dataUser.email}</td>
            <td>${(dataUser.admin ? 'Sim' : 'N&atilde;o' )}</td>
            <td>${Utils.dateFormat(dataUser.register)}</td>
            <td>
                <button type="button" class="btn btn-primary btn-edit btn-xs btn-flat">Editar</button>
                <button type="button" class="btn btn-danger btn-delete btn-xs btn-flat">Excluir</button>
            </td>
        `;
        this.addEventsTr(tr);
        return tr;
    }

    addEventsTr(tr) {
        tr.querySelector(".btn-delete").addEventListener("click", e => {
            if (confirm("Deseja realmente excluir?")) {
                let user = new User();
                user.loadFromJSON(JSON.parse(tr.dataset.user));
                user.delete(); // remove do localStorage
                tr.remove(); // remove do formulário HTML.
                this.updateCount(); // Atualiza o contador do formulário HTML.
            }
        });
        tr.querySelector(".btn-edit").addEventListener("click", e => {
            let json = JSON.parse(tr.dataset.user);
            // let form = document.querySelector("#form-user-update");
            this.formUpdateE1.dataset.trIndex = tr.sectionRowIndex; // pegar o índice do array de usuário, porque não existe ainda o Id do registro no banco de dados.
            for (let name in json) {
                let field = this.formUpdateE1.querySelector("[name=" + name.replace("_", "") + "]");
                if (field) {
                    switch (field.type) {
                        case 'file':
                            continue;
                            break;
                        case 'radio':
                            field = this.formUpdateE1.querySelector("[name=" + name.replace("_", "") + "][value=" + json[name] + "]");
                            field.checked = true;
                            break;
                        case 'checkbox':
                            field.checked = json[name];
                        default:
                            field.value = json[name];
                    }
                }
            }
            this.formUpdateE1.querySelector(".photo").src = json._photo;
            this.showPanelUpdate();
        });
    }

    showPanelCreate() {
        document.querySelector("#box-user-create").style.display = "block"; // mostra este bloco de formulário.
        document.querySelector("#box-user-update").style.display = "none"; //  esconde este bloco de formulário.
}

    showPanelUpdate() {
        document.querySelector("#box-user-create").style.display = "none"; // esconde este bloco de formulário.
        document.querySelector("#box-user-update").style.display = "block"; // mostra este bloco de formulário.
}

    updateCount() {
        let numberUsers = 0;
        let numberAdmin = 0;

        [...this.tableE1.children].forEach(tr =>{
            numberUsers++;
            let user = JSON.parse(tr.dataset.user); //UnSerialize JSON.
            if (user._admin) numberAdmin++;
        });

        document.querySelector("#number-users").innerHTML = numberUsers;
        document.querySelector("#number-users-admin").innerHTML = numberAdmin;
    }
}