/*
 * File: index.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Sunday, 12 April 2020 17:08:00
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Saturday, 18 April 2020 15:07:00
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */
let userController = new UserController("form-user-create","form-user-update", "table-users"); // apontando o UserController para o trecho do formulário que será tratado.
