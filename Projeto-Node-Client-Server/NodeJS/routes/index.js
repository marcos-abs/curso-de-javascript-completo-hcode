/*
 * File: index.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Tuesday, 21 April 2020 11:57:24
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Tuesday, 21 April 2020 12:22:41
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */


module.exports = (app) => {
    app.get('/', (req, res) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/html'); // Header do HTML dizendo que tipo de informação será passada pelo o servidor.
        res.end('<h1>Ol&aacute;</h1>');
    });
}
