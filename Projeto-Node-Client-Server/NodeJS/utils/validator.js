/*
 * File: validator.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Wednesday, 22 April 2020 05:54:02
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Wednesday, 22 April 2020 05:54:03
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

 module.exports = {
    user:(app, req, res) => {
        req.assert('name', 'O nome &eacute; obrigat&oacute;rio.').notEmpty();
        req.assert('email', 'O endere&ccedil;o de e-mail est&aacute; inv&aacute;lido.').notEmpty().isEmail();
        let errors = req.validationErrors();
        if (errors) {
            app.utils.error.send(errors, req, res);
            return false;
        } else {
            return true;
        }
    }
 };