/*
 * File: users.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Thursday, 23 April 2020 11:49:05
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Monday, 27 April 2020 18:14:07
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

var express = require('express');
var restify = require('restify-clients');
var assert = require('assert');
var router = express.Router();

// Restify-Client - Creates a JSON client
var client = restify.createJsonClient({
  url: 'http://192.168.56.107:4000' // endereço do servidor onde consumirá os serviços REST.
});

/* GET users listing. */
router.get('/', function(req, res, next) {

  // res.send('respond with a resource');
  // client.basicAuth('$login', '$password'); // não há autenticação (por enquanto).

  client.get('/users', function(err, request, response, obj) {

    assert.ifError(err);

    res.json(obj);

  });

});

router.get('/:id', function(req, res, next) {

  client.get(`/users/${req.params.id}`, function(err, request, response, obj) {

    assert.ifError(err);

    res.json(obj);

  });

});

router.put('/:id', function(req, res, next) {

  client.put(`/users/${req.params.id}`, req.body, function(err, request, response, obj) {

    assert.ifError(err);

    res.json(obj);

  });

});

router.post('/', function(req, res, next) {

  client.post('/users', req.body, function(err, request, response, obj) {

    assert.ifError(err);

    res.json(obj);

  });

});

router.delete('/:id', function(req, res, next) {

  client.del(`/users/${req.params.id}`, function(err, request, response, obj) {

    assert.ifError(err);

    res.json(obj);

  });

});

module.exports = router;
