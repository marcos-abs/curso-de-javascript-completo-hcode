/*
 * File: index.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Saturday, 11th April 2020 1:12:29 am
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Saturday, 11th April 2020 1:12:58 am
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

 let carros = ["palio 98", "toro", "uno", 10, true, new Date(), function(){}];

carros.forEach(function(value, index) {
    console.log(index, value);
});