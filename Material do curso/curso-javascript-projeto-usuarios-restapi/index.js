/*
 * File: index.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Monday, 27 April 2020 17:59:51
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Monday, 27 April 2020 18:14:52
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

 const express = require('express');
const consign = require('consign');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');

let app = express();

app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' })); // aumento do tamanho do post para o express framework suportar URLs maiores para as fotos (por exemplo).
app.use(bodyParser.json({ limit: '50mb' })); // idem ao anterior
app.use(expressValidator());

consign().include('routes').include('utils').into(app);

app.listen(4000, '192.168.56.107', ()=>{ // endereço IP da minha VM.

    console.log('servidor rodando!');

});