/*
 * File: Fetch.js
 * Project: Curso de Javascript Completo - Hcode
 * File Created: Sunday, 26 April 2020 13:49:58
 * Author: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br)
 * -----
 * Last Modified: Tuesday, 28 April 2020 13:26:54
 * Modified By: Marcos Antônio B. de Souza (desouza.marcos@uol.com.br>)
 * -----
 * Copyright 2020 - 2020 All rights reserved, Marcant Tecnologia
 */

class Fetch {

    static get(url, params = {}) {

        return Fetch.request('GET', url, params);

    }

    static delete(url, params = {}) {

        return Fetch.request('DELETE', url, params);

    }

    static put(url, params = {}) {

        return Fetch.request('PUT', url, params);

    }

    static post(url, params = {}) {

        return Fetch.request('POST', url, params);

    }

    static request(method, url, params = {}) {

        return new Promise((resolve, reject) => {

            let request;

            switch (method.toLowerCase()) {
                case 'get':
                    request = url;
                break;
                default:
                    request = new Request(url, {

                        method, // method = method => method somente.
                        body: JSON.stringify(params),
                        headers: new Headers({
        
                            'Content-Type': 'application/json'
        
                        })
        
                    });

                }

            fetch(request).then(response => {

                response.json().then(json => {

                    resolve(json);

                }).catch(e => {

                    reject(e);

                });

            }).catch(e => {

                reject(e);

            });
    
        });

    }
}